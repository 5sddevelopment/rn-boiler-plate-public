# REACT NATIVE BOILER PLATE

In order to use this boiler plate in your project, You need to create latest react-native project and then copy-paste/replace the following items in your project.

`./src`
`./index.js`
`./App.js`
`./react-native.config.js`

After adding the above items to your project. 

Remove the following folders/files before moving forward.
`./node_modules`
`./yarn.lock` or `package-lock.json`
`./ios/Pods`
`./ios/Podfile.lock`

Make sure to install the following libraries in your project through npm/yarn.

Run the following command in your root folder. 

```yarn add @react-native-async-storage/async-storage @react-navigation/native @react-navigation/native-stack react-native-screens react-native-safe-area-context axios react-native-vector-icons react-native-gesture-handler react-redux redux redux-logger redux-saga react-native-toast-message @react-native-community/netinfo react-native-image-resizer react-native-image-picker react-native-keyboard-aware-scroll-view react-native-fast-image react-native-raw-bottom-sheet @react-native-community/datetimepicker react-native-permissions react-native-geolocation-service react-native-maps && cd ios && pod install && cd .. && npx react-native link```

Add below font-family in Info.plist to add Vector Icon fonts and avoid  ***Unrecognized Font Famliy Error***;

```<string>AntDesign.ttf</string>```
```<string>Entypo.ttf</string>```
```<string>EvilIcons.ttf</string>```
```<string>Feather.ttf</string>```
```<string>FontAwesome.ttf</string>```
```<string>FontAwesome5_Brands.ttf</string>```
```<string>FontAwesome5_Regular.ttf</string>```
```<string>FontAwesome5_Solid.ttf</string>```
```<string>Foundation.ttf</string>```
```<string>Ionicons.ttf</string>```
```<string>MaterialIcons.ttf</string>```
```<string>MaterialCommunityIcons.ttf</string>```
```<string>SimpleLineIcons.ttf</string>```
```<string>Octicons.ttf</string>```
```<string>Zocial.ttf</string>```
```<string>Fontisto.ttf</string>```

Add below font-family in android/app/build.gradle if fonts are not displaying in android

```project.ext.vectoricons = [ iconFontNames: ['Fontisto.ttf', 'Zocial.ttf','Octicons.ttf', 'SimpleLineIcons.ttf', 'Foundation.ttf', 'FontAwesome5_Solid.ttf', 'FontAwesome5_Regular.ttf', 'FontAwesome5_Brands.ttf', 'Feather.ttf', 'EvilIcons.ttf', 'MaterialIcons.ttf', 'FontAwesome.ttf', 'MaterialCommunityIcons.ttf', 'Ionicons.ttf', 'AntDesign.ttf', 'Entypo.ttf']]```

```apply from: "../../node_modules/react-native-vector-icons/fonts.gradle"```


## Make sure to add following permissions ios and android folders to access camera, gallery and location services.

Add below line at after target in ios/Podfile for specifying permission path.
```permissions_path = '../node_modules/react-native-permissions/ios'```


#### Access Camera and Gallery
***
***For iOS***
in ios/Podfile:
  ```pod 'Permission-Camera', :path => "#{permissions_path}/Camera"```
  ```pod 'Permission-PhotoLibrary', :path => "#{permissions_path}/PhotoLibrary"```
  ```pod 'Permission-PhotoLibraryAddOnly', :path => "#{permissions_path}/PhotoLibraryAddOnly"```
  ```pod 'Permission-Microphone', :path => "#{permissions_path}/Microphone" ```
  
in Info plist:
   ```<key>NSCameraUsageDescription</key>```
   ```<string> Camera Usage Description </string>```
   ```<key>NSMicrophoneUsageDescription</key>```
   ```<string> Microphone Usage Description </string>```
   ```<key>NSPhotoLibraryUsageDescription</key>```
   ```<string> Photo Library Usage Description </string>```
   
 ***For Android***
 in AndroidManifest.xml
 ```<uses-permission android:name="android.permission.CAMERA" />```
 ```<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>```
 ```<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />```
 
#### Access Location
 *** 
***For iOS***
in ios/Podfile:
 ```pod 'Permission-LocationAccuracy', :path => "#{permissions_path}/LocationAccuracy"```
 ```pod 'Permission-LocationAlways', :path => "#{permissions_path}/LocationAlways"```
 ```pod 'Permission-LocationWhenInUse', :path => "#{permissions_path}/LocationWhenInUse"```
  
in Info plist:
   ```<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>```
  ```<string> Location Usage Description </string>```
  ```<key>NSLocationAlwaysUsageDescription</key>```
  ```<string> Location Usage Description </string>```
  ```<key>NSLocationUsageDescription</key>```
  ```<string> Location Usage Description </string>```
  ```<key>NSLocationWhenInUseUsageDescription</key>```
  ```<string> Location Usage Description </string>```
   
 ***For Android***
 in AndroidManifest.xml
 ```<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />```

***
#####  ***MapView***
In order to use MapView in android you need to add below key with your Google Maps API key in AndroidManifest.xml
``` <meta-data android:name="com.google.android.geo.API_KEY" android:value="YOUR-API-KEY"/> ```
***

## NOTE : Make sure to strictly follow the define pattern of the boiler plate in your project. Must go through the structure of the project before starting the project.

1. END POINTS SHOULD BE DEFINED IN VARIABLES' FILE IN CONFIG FOLDER `./src/config/variables/index.js`
2. ALL REUSABLE FUNCTIONS SHOULD BE DEFINED IN UTIL FOLDER `./src/config/util/index.js`
3. ALL FONTS & COLORS SHOULD BE PREDEFINED IN THE RESPECTIVE FOLDERS `./src/config`
4. ALL NAVIGATORS SHOULD BE DEFINED IN NAVIGATION CONFIG FOLDER `./src/navigationConfig`
5. ALL ACTIONS, REDUCERS, MIDDLEWARES SAGAS & CONSTANTS SHOULD BE DEFINED IN RESPECTIVE FOLDERS `./src/store`
6. ALL REUSABLE COMPONENTS SUCH AS BUTTONS, TEXTFIELD etc. SHOULD BE DEFINED IN THE COMPONENTS FOLDER WITH PREDEFINED PROPS WITH DEFAULT VALUES. ALL COMPONENTS SHOULD BE WRAPPED IN REACT MEMO `React.memo(Component)` IN ORDER TO PREVENT RERENDERING (refer to pre-created components) `./src/components`
7. Always use KeyboardAwareScrollView instead of ScrollView to avoid window keyboard clashing in iOS Devices.